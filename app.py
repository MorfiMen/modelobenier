import warnings
import os
import numpy as np
from numpy import array
from flask import jsonify
import pandas as pd
from pandas import DataFrame
from pandas import concat
import os.path as path
from flask import Response
import csv
import json
from datetime import datetime

from flask import Response
from flask import Flask
app = Flask(__name__)

wsgi_app = app.wsgi_app

class Prediction:
    def __init__(self, date, inflation, cpi, ppi, external_debt, rrm):
        self.date = date
        self.inflation = inflation
        self.cpi = cpi
        self.external_debt = external_debt
        self.rrm = rrm

@app.route('/')
def predictions():
    prediction_list = []

    headers = {
        'Content-Type': 'application/json',
    }

    df = pd.read_csv('data/predictions.csv')
    for i in range(0, len(df)):
        prediction_list.append(Prediction(str(df['Date'][i]), float(df['Inflation'][i]), float(df['CPI'][i]), float(df['PPI'][i]), float(df['External_debt'][i]), float(df['RRM'][i])))

    list_string_json = json.dumps([ob.__dict__ for ob in prediction_list])
    return Response(list_string_json, mimetype="application/json")

@app.route('/datos')
def datos():
    data_list = []
    df = pd.read_csv('data/data.csv')
    for i in range(0, len(df)):
        data_list.append(Prediction(str(df['Date'][i]), float(df['Inflation'][i]), float(df['CPI'][i]), float(df['PPI'][i]), float(df['External_debt'][i]), float(df['RRM'][i])))

    list_string_json = json.dumps([ob.__dict__ for ob in data_list])
    return Response(list_string_json, mimetype="application/json")


if __name__ == '__main__':
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 55555
    app.run(HOST, PORT)
